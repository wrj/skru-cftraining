<cfset result = application.contactApp.listContacts()>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Contacts</title>
</head>
<body>
<cfoutput>
    <h1>Contacts List</h1>
    <table border="1" cellpadding="2">
        <tr>
            <td>ID</td>
            <td>Full Name</td>
            <td>Email</td>
            <td>
            <a href="add_contact.cfm">
            [Add New]</a>
            </td>
        </tr>
    <cfloop query="result">
        <tr>
            <td>#result.contactID#</td>
            <td>
            <a href="edit_contact.cfm?contactID=#result.contactID#">
            #result.firstName# #result.lastName#</a>
            </td>
            <td>#result.email#</td>
            <td>
            <a href="do_delete_contact.cfm?contactID=#result.contactID#">
            [Delete]</a>
            </td>
        </tr>
    </cfloop>
    </table>
</cfoutput>
</body>
</html>