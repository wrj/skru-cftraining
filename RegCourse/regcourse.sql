/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50144
 Source Host           : localhost
 Source Database       : regcourse

 Target Server Type    : MySQL
 Target Server Version : 50144
 File Encoding         : utf-8

 Date: 11/14/2013 13:16:13 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `course`
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `description` text,
  `status` int(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `course`
-- ----------------------------
BEGIN;
INSERT INTO `course` VALUES ('1', 'Math', '423101', 'คณิตศาสตร์', '0'), ('2', 'Physics', '423102', 'ฟิสิกส์', '0');
COMMIT;

-- ----------------------------
--  Table structure for `coursestudentjoin`
-- ----------------------------
DROP TABLE IF EXISTS `coursestudentjoin`;
CREATE TABLE `coursestudentjoin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(4) NOT NULL,
  `course_id` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `coursestudentjoin`
-- ----------------------------
BEGIN;
INSERT INTO `coursestudentjoin` VALUES ('1', '1', '1'), ('2', '1', '2'), ('3', '2', '1');
COMMIT;

-- ----------------------------
--  Table structure for `student`
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `gender` varchar(5) NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `status` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `student`
-- ----------------------------
BEGIN;
INSERT INTO `student` VALUES ('1', 'Piyawat', 'Damrongsuphakit', 'arunwatd@gmail.com', 'M', '', null), ('2', 'Weerasak', 'Chongnguluam', 'singpor@gmail.com', 'M', null, null);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
