component {
	function setUp() {
		RegCourse = new RegCourse.RegCourse();

	}

	function tearDown() {
		query datasource="#application.datasource#" {
			echo("delete from student where id > 2;");
			
		};
		
		query datasource="#application.datasource#" {
			echo("delete from course where id > 2;");
		};

		query datasource="#application.datasource#" {
			echo("delete from coursestudentjoin where id > 3;");
		};		
	}
	function testListStudent() {
		var result = RegCourse.listStudent();
		$assert.typeOf("query", result);
		$assert.isEqual(result.recordCount, 2);
	}

	function testGetStudent() {
		var result = RegCourse.getStudent(1);
		$assert.typeOf("query", result);
		$assert.isEqual(result.recordCount, 1);
		$assert.isEqual(result.firstname, "Piyawat");
	}

	function testUpdateStudent() {
		RegCourse.updateStudent({
			"id" = 1,
			"firstname" = "Piyawat",
			"lastname" = "Damrongsuphakit",
			"email" = "arunwatd@gmail.com",
			"gender" = "M",
			"photo" = "piyawat.jpg"
		});

		var result = RegCourse.getStudent(1);
		$assert.typeOf("query", result);
		$assert.isEqual(result.recordCount, 1);
		$assert.isEqual(result.photo, "piyawat.jpg");

		RegCourse.updateStudent({
			"id" = 1,
			"firstname" = "Piyawat",
			"lastname" = "Damrongsuphakit",
			"email" = "arunwatd@gmail.com",
			"gender" = "M",
			"photo" = ""
		});
	}

	function testAddStudent() {
		RegCourse.addStudent({
			"firstname" = "addfirstname",
			"lastname" = "addlastname",
			"email" = "addemail",
			"gender" = "M",
			"photo" = ""
		});
		var result = RegCourse.listStudent();
		$assert.typeOf("query", result);
		$assert.isEqual(result.recordCount, 3);

	}


	function testListCourse() {
		var result = RegCourse.listCourse();
		$assert.typeOf("query", result);
		$assert.isEqual(result.recordCount, 2);
	}

	function testGetCourse() {
		var result = RegCourse.getCourse(1);
		$assert.typeOf("query", result);
		$assert.isEqual(result.recordCount, 1);
		$assert.isEqual(result.name, "Math");
	}

	function testUpdateCourse() {
		RegCourse.updateCourse({
			"id" = 1,
			"name" = "Social",
			"code" = "423101",
			"description" = "คณิตศาสตร์"
		});

		var result = RegCourse.getCourse(1);
		$assert.typeOf("query", result);
		$assert.isEqual(result.recordCount, 1);
		$assert.isEqual(result.name, "Social");

		RegCourse.updateCourse({
			"id" = 1,
			"name" = "Math",
			"code" = "423101",
			"description" = "คณิตศาสตร์"
		});
	}

	function testAddCourse() {
		RegCourse.addCourse({
			"name" = "Thai",
			"code" = "423111",
			"description" = "ภาษาไทย"
		});
		var result = RegCourse.listCourse();
		$assert.typeOf("query", result);
		$assert.isEqual(result.recordCount, 3);
	}

	function testGetCourseFromStudent() {
		var result = RegCourse.getCourseFromStudent(1);
		$assert.typeOf("query", result);
		$assert.isEqual(result.recordCount, 2);
		$assert.isEqual(result['name'][1], "Math");
	}

	function testGetStudentFromCouse() {
		var result = RegCourse.getStudentFromCourse(1);
		$assert.typeOf("query", result);
		$assert.isEqual(result.recordCount, 2);
		$assert.isEqual(result['firstname'][1], "Piyawat");
	}

	function testTakeCourse() {
		var StudentId = 2;
		var CourseId = 2;
		RegCourse.takeCourse(StudentId, CourseId);

		var result = RegCourse.getStudentFromCourse(2);
		$assert.typeOf("query", result);
		$assert.isEqual(result.recordCount, 2);
		$assert.isEqual(result['firstname'][2], "Weerasak");
	}

	function testDeleteCourse() {
		RegCourse.addCourse({
			"name" = "Thai",
			"code" = "423111",
			"description" = "ภาษาไทย"
		});
		var result = RegCourse.listCourse();
		var CourseId = result['id'][3];
		var StudentId = 2;

		RegCourse.takeCourse(StudentId, CourseId);
		
		RegCourse.deleteCourse(CourseId);

		result = RegCourse.getCourse(CourseId);
		$assert.isEqual(result.recordCount, 0);

		result = RegCourse.getStudentFromCourse(CourseId);
		$assert.isEqual(result.recordCount, 0);
	}

	function testDeleteStudent() {
		RegCourse.addStudent({
			"firstname" = "addfirstname",
			"lastname" = "addlastname",
			"email" = "addemail",
			"gender" = "M",
			"photo" = ""
		});
		var result = RegCourse.listStudent();
		var StudentId = result['id'][3];
		var CourseId = 2;
		RegCourse.takeCourse(StudentId, CourseId);

		RegCourse.deleteStudent(StudentId);

		result = RegCourse.getStudent(StudentId);
		$assert.isEqual(result.recordCount, 0);

		result = RegCourse.getCourseFromStudent(StudentId);
		$assert.isEqual(result.recordCount, 0);

	}

	function testCancelCourse() {
		var StudentId = 2;
		var CourseId = 2;
		RegCourse.takeCourse(StudentId, CourseId);

		var result = RegCourse.getStudentFromCourse(2);
		$assert.typeOf("query", result);
		$assert.isEqual(result.recordCount, 2);
		$assert.isEqual(result['firstname'][2], "Weerasak");

		RegCourse.cancelCourse(StudentId, CourseId);
		result = RegCourse.getCourseFromStudent(2);
		$assert.typeOf("query", result);
		$assert.isEqual(result.recordCount, 1);
	}

}