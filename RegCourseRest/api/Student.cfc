component restpath="/student" rest="true"{
	
	remote any function listStudent() httpmethod="GET" {
		var result = application.regcourse.listStudent();
		var resultArray = [];
		var student = {};
		for (var index = 1; index <= result.recordCount; index+=1) {
			student = {
				"id" : result.id[index],
				"firstname" : result.firstname[index],
				"lastname" : result.lastname[index],
				"email" : result.email[index],
				"gender" : result.gender[index],
				"photo" : result.photo[index]
			}
			arrayAppend(resultArray,student);
		}
		return resultArray;
	}

	remote any function getStudent(numeric id restargsource="PATH") httpmethod="GET" restpath="{id}" {
		var result = application.regcourse.getStudent(arguments.id);
		if (result.recordCount neq 0) {
			return {
			"id" : result.id,
			"firstname" : result.firstname,
			"lastname" : result.lastname,
			"email" : result.email,
			"gender" : result.gender,
			"photo" : result.photo
		};
		}
		
		return {};
	}

	remote any function addStudent(
					any firstname		restargsource="Form",
					any lastname		restargsource="Form",
					any email				restargsource="Form",
					any gender			restargsource="Form",
					any photoFile		restargsource="Form",
				) httpmethod="POST" restpath="add"{
		var photo = '';
		var uploadResult = {};
		if (arguments.photoFile neq "") {
			file 
				action="upload" 
				destination="#expandPath('/RegCourseRest/img')#"
				filefield="photoFile"
				result="uploadResult"
				nameconflict="makeunique";
			photo = uploadResult.serverFile;
		}
		application.regcourse.addStudent({
			'firstname'	: arguments.firstname,
			'lastname'	: arguments.lastname,
			'email'			: arguments.email,
			'gender'		: arguments.gender,
			'photo'			: photo
			})
		return true;
	}

	remote any function updateStudent(
					any id 					restargsource="Form",
					any firstname		restargsource="Form",
					any lastname		restargsource="Form",
					any email				restargsource="Form",
					any gender			restargsource="Form",
					any photo 			restargsource="Form",
					any photoFile		restargsource="Form",
				) httpmethod="POST" restpath="update" {

		var uploadResult = {};
		if (arguments.photoFile neq "") {
			file 
				action="upload" 
				destination="#expandPath('/RegCourseRest/img')#"
				filefield="photoFile"
				result="uploadResult"
				nameconflict="makeunique";
			arguments.photo = uploadResult.serverFile;
		}
		application.regcourse.updateStudent({
			'id'				: arguments.id,
			'firstname'	: arguments.firstname,
			'lastname'	: arguments.lastname,
			'email'			: arguments.email,
			'gender'		: arguments.gender,
			'photo'			: arguments.photo
			})
		return true;
	}

	remote any function deleteStudent(
					any id 					restargsource="PATH"
				) httpmethod="GET" restpath="{id}/delete" {
		application.regcourse.deleteStudent(arguments.id);
		return true;
	}

	remote any function getCourses(
					any id 					restargsource="PATH"
				) httpmethod="GET" restpath="{id}/courses" {
		var result = application.regcourse.getCourseFromStudent(arguments.id);
		var resultArray = [];
		var course = {};
		for (var index = 1; index <= result.recordCount; index+=1) {
			course = {
				"id" : result.id[index],
				"code" : result.code[index],
				"name" : result.name[index],
				"description" : result.description[index]
			}
			arrayAppend(resultArray,course);
		}
		return resultArray;
	}

	remote any function takeCourse(
				any id 					restargsource="PATH",
				any course_id		restargsource="PATH"
			) httpmethod="POST" restpath="{id}/take/{course_id}" {
		application.regcourse.takeCourse(arguments.id, arguments.course_id);
		return true;
	}

	remote any function cancelCourse(
				any id 					restargsource="PATH",
				any course_id		restargsource="PATH"
			) httpmethod="GET" restpath="{id}/cancel/{course_id}" {
		application.regcourse.cancelCourse(arguments.id, arguments.course_id);
		return true;
	}
}