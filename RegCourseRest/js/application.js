    $(document).foundation();

    var closeAllSection = function() {
      $("#list_all_section").css("display","none");
      $("#show_student_section").css("display","none");
      $("#show_course_section").css("display","none");
      $("#student_form_section").css("display","none");
      $("#course_form_section").css("display", "none");
      $("#register_form").css("display", "none");
    }

    var registerForm = function() {
      closeAllSection();
      $("#register_form").css("display", "");
      $("#register_form_header").html("Register Course");
      $("#register_form select[name=course_id]").html('');
      $("#register_form select[name=student_id]").html('');

      $.ajax({
        url : '/rest/regcourse/api/course',
        dataType: 'json'
      }).done(function(courses){
        for(var index = 0; index < courses.length; ++index) {
          $("#register_form select[name=course_id]").append('<option value="' + courses[index].id + '">' + courses[index].name + '</option>')
        }
      })

      $.ajax({
        url : '/rest/regcourse/api/student',
        dataType: 'json'
      }).done(function(students){
        for(var index = 0; index < students.length; ++index) {
          $("#register_form select[name=student_id]").append('<option value="' + students[index].id + '">' + students[index].firstname + ' ' + students[index].lastname + '</option>') 
        }
      })
    }

    var insertCourseForm = function() {
      closeAllSection();
      $("#course_form_section input[type=text]").val('');
      $("#course_form_header").html("Insert New Course");
      $("#course_form_section form").attr("action", "/rest/regcourse/api/course/add");
      $("#course_form_section form input[type=submit]").val("Create");
      $("#course_form_section").css("display","");
    }

    var editCourseForm = function(course_id) {
      closeAllSection();
      $("#course_form_header").html("Edit Course");
      $("#course_form_section form input[type=submit]").val("Update");
      $("#course_form_section form").attr("action", "/rest/regcourse/api/course/update");
      $("#course_form_section").css("display","");

      $.ajax({
        url: '/rest/regcourse/api/course/' + course_id,
        dataType: 'json'
      }).done(function(course) {
        $("#course_form_section form input[name=id]").val(course.id);
        $("#course_form_section form input[name=code]").val(course.code);
        $("#course_form_section form input[name=name]").val(course.name);
        $("#course_form_section form input[name=description]").val(course.description);
      })
    }

    var insertStudentForm = function() {
      closeAllSection();
      $("#student_form_section input[type=text]").val('');
      $("#student_form_section input[type=email]").val('');
      $("#student_form_section input[value=M]").prop("checked",true);
      $("#student_form_section input[value=F]").prop("checked",false);
      $("#student_form_header").html("Insert New Student");
      $("#student_form_section form").attr("action","/rest/regcourse/api/student/add");
      $("#student_form_section form input[type=submit]").val("Create");
      $("#student_form_section").css("display", "");
    }

    var editStudentForm = function(student_id) {
      closeAllSection();
      $("#student_form_header").html("Edit Student");
      $("#student_form_section form input[type=submit]").val("Update");      
      $("#student_form_section form").attr("action","/rest/regcourse/api/student/update");
      $("#student_form_section").css("display", "");
      $.ajax({
        url: '/rest/regcourse/api/student/' + student_id,
        dataType: 'json'
      }).done(function(student) {
        $("#student_form_section input[name=id]").val(student.id);
        $("#student_form_section input[name=firstname]").val(student.firstname);
        $("#student_form_section input[name=lastname]").val(student.lastname);
        $("#student_form_section input[name=email]").val(student.email);

        if (student.gender == 'M') {
          $("#student_form_section input[value=M]").prop("checked", true);
          $("#student_form_section input[value=F]").prop("checked", false);
        } else if (student.gender == 'F') {
          $("#student_form_section input[value=M]").prop("checked", false);
          $("#student_form_section input[value=F]").prop("checked", true);
        }
        $("#student_form_section input[name=photo]").val(student.photo);
        $("#student_form_section input[name=photoFile]").val('');
      });
    }

    var cancelCourse = function () {
      $.ajax({
        url: '/rest/regcourse/api/student/' + $(this).data("student_id") +  '/cancel/' + $(this).data('course_id'),
        type: 'GET',
        contentType: false,
        cache: false,
        processData:false
      }).done(function() {
        listAll();
      })
    }

    var showCourse = function() {
      closeAllSection();
      $("#show_course_section").css("display", "");
      $.ajax({
        url: '/rest/regcourse/api/course/' + $(this).data("id"),
        dataType: 'json'
      }).done(function(course) {
        $("#course_code").html(course.code);
        $("#course_name").html(course.name);
        $("#course_description").html(course.description);
        
        
        $("#course_edit_button").data("id", course.id);
        $("#course_delete_button").data("id", course.id);

        $.ajax({
          url: '/rest/regcourse/api/course/' + course.id + '/students',
          dataType: 'json'
        }).done(function (students) {
          $("#course_student_table tbody").html('');
          for (var index = 0; index < students.length; ++index) {
            var row = $("<tr>");
            var firstname = $("<td>");
            var lastname = $("<td>");
            var show_link = $("<a>");
            show_link.append(students[index].firstname);
            show_link.attr("href", "#");
            show_link.data("id", students[index].id);
            show_link.on("click",showStudent);
            firstname.append(show_link)
            lastname.append(students[index].lastname);
            row.append(firstname);
            row.append(lastname);
            $("#course_student_table tbody").append(row);
          }
        })
      })
    }

    var showStudent = function() {
      closeAllSection();
      $("#show_student_section").css("display", "");
      $.ajax({
        url: '/rest/regcourse/api/student/' + $(this).data("id"),
        dataType: 'json'
      }).done(function(student) {
        $("#student_name").html(student.firstname + ' ' + student.lastname);
        $("#student_email").html(student.email);
        $("#student_gender").html("Gender : " + student.gender);
        
        var img = $("<img>");
        $("#student_photo").html(img);
        img.attr("src","img/" + student.photo);
        img.css("width", "100px");
        img.css("height", "100px");
        
        $("#student_edit_button").data("id", student.id);
        $("#student_delete_button").data("id", student.id);

        $.ajax({
          url: '/rest/regcourse/api/student/' + student.id + '/courses',
          dataType: 'json'
        }).done(function (courses) {
          $("#student_courses_table tbody").html('');
          for (var index = 0; index < courses.length; ++index) {
            var row = $("<tr>");
            var code = $("<td>");
            var name = $("<td>");
            var action = $("<td>");
            var cancel_link = $("<a>");
            var show_link = $("<a>");
            var description = $("<td>");

            code.append(courses[index].code);
            show_link.data("id", courses[index].id);
            show_link.attr("href", "#");
            show_link.append(courses[index].name)
            name.append(show_link);
            show_link.on("click",showCourse)
            description.append(courses[index].description)
            cancel_link.data("student_id", student.id);
            cancel_link.data("course_id", courses[index].id);
            cancel_link.attr("href","#");
            cancel_link.append("ถอน");
            cancel_link.on("click", cancelCourse);
            action.append(cancel_link);
            row.append(code);
            row.append(name);
            row.append(description);
            row.append(action);
            $("#student_courses_table tbody").append(row);
          }
        })
      })
    }

    var listCourse = function () {
      $.ajax({
        url: '/rest/regcourse/api/course',
        dataType: 'json'
      }).done(function(courses) {
        $("#courses_table tbody").html('');
        for (var index = 0; index < courses.length; ++index) {
          var row = $("<tr>");
          var code = $("<td>");
          var name = $("<td>");
          var show_link = $("<a>");
          var description = $("<td>");
          code.append(courses[index].code);
          show_link.data("id", courses[index].id);
          show_link.attr("href", "#");
          show_link.append(courses[index].name)
          name.append(show_link);
          show_link.on("click",showCourse)
          description.append(courses[index].description)
          row.append(code);
          row.append(name);
          row.append(description);
          $("#courses_table tbody").append(row);
        }
      })
    }

    var listStudent = function() {
      $.ajax({
        url: '/rest/regcourse/api/student',
        dataType: 'json'
      }).done(function(students) {
        $("#students_table tbody").html('');
        for (var index = 0; index < students.length; ++index) {
          var row = $("<tr>");
          var firstname = $("<td>");
          var lastname = $("<td>");
          var show_link = $("<a>");
          show_link.append(students[index].firstname);
          show_link.attr("href", "#");
          show_link.data("id", students[index].id);
          show_link.on("click",showStudent);
          firstname.append(show_link)
          lastname.append(students[index].lastname);
          row.append(firstname);
          row.append(lastname);
          $("#students_table tbody").append(row);
        }
      })
    }

    var listAll = function () {
      closeAllSection();
      $('#list_all_section').css("display","");
      listCourse();
      listStudent();
    }

    listAll();
    

    $("#list_all_button").on("click", function() {
      listAll();
    })

    $("#student_delete_button").on("click", function() {
      var student_id = $(this).data("id");
      if (confirm("ต้องการลบ student ใช่หรือไม่?")) {
        $.ajax({
          url:'/rest/regcourse/api/student/' + student_id + '/delete',
          contentType: false,
          cache: false,
          processData:false
        }).done(function() {
          listAll();
        })
      }
    })

    $("#student_edit_button").on("click", function() {
      var student_id = $(this).data("id");
      editStudentForm(student_id);
      
    })

    $("#course_delete_button").on("click", function() {
      var course_id = $(this).data("id");
      if (confirm("ต้องการลบ course ใช่หรือไม่?")) {
        $.ajax({
          url:'/rest/regcourse/api/course/' + course_id + '/delete',
          contentType: false,
          cache: false,
          processData:false
        }).done(function() {
          listAll();
        })
      }
    })

    $("#course_edit_button").on("click", function() {
      editCourseForm($(this).data("id"));
    })

    $("#add_student_button").on("click", function() {
      insertStudentForm();
    })

    $("#add_course_button").on("click", function() {
      insertCourseForm();
    })

    $("#student_form_section form").on("submit", function(e) {
      var formObj = $(this);
      var formURL = $(this).attr("action");
      var formData = new FormData(this);
      $.ajax({
        url: formURL,
        type: 'POST',
        data: formData,
        mimeType: 'multipart/form-data',
        contentType: false,
        cache: false,
        processData:false
      }).done(function() {
        listAll();
      })
      e.preventDefault();
      return false;
    })

    $("#course_form_section form").on("submit", function(e) {
      var formObj = $(this);
      var formURL = $(this).attr("action");
      var formData = new FormData(this);
      $.ajax({
        url: formURL,
        type: 'POST',
        data: formData,
        contentType: false,
        cache: false,
        processData:false
      }).done(function() {
        listAll();
      })
      e.preventDefault();
      return false;
    })


    $("#register_button").on("click", function() {
      registerForm();
    })

    $("#register_form form").on("submit", function(e) {
      var student_id = $("#register_form form select[name=student_id]").val();
      var course_id = $("#register_form form select[name=course_id]").val();

      $.ajax({
        url: '/rest/regcourse/api/student/' + student_id + '/take/' + course_id,
        type: 'POST',
        contentType: false,
        cache: false,
        processData:false
      }).done(function() {
        listAll();
      })
      e.preventDefault();
      return false;
    })