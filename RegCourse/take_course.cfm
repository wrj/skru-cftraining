<!DOCTYPE html>
<!--[if IE 8]> 				 <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->

<cfset courses = application.regcourse.listCourse()>
<cfset students = application.regcourse.listStudent()>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <title>Insert New Course</title>
    <link rel="stylesheet" href="css/foundation.css">
    <script src="js/vendor/custom.modernizr.js"></script>
</head>
<body>

<div class="row">
    <div class="large-12 columns">
        <h2>Welcome to Register Course System</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad amet, deserunt doloribus eos error ex ipsum laudantium nostrum numquam odio provident quibusdam tempore tenetur unde ut vero vitae! Eum, facilis.</p>
        <hr />
        <div class="row">
            <div class="large-8 large-offset-2 columns">
                <h5>Insert New Course</h5>
                <div class="form-for-new-student">
                    <form action="do_take_course.cfm" method="POST">
                        <label for="course_id">Course</label>
                        <select name="course_id">
                            <cfoutput query="courses">
                                <option value="#courses.id#">#courses.name#</option>
                            </cfoutput>
                        </select>
                        <label for="student_id">Student</label>
                        <select name="student_id">
                            <cfoutput query="students">
                                <option value="#students.id#">#students.firstname# #students.lastname#</option>
                            </cfoutput>
                        </select>
                        <div style="margin-top: 12px;">
                            <input class="nice radius button" type="submit" name="submitButton" value="Register">
                            <div class="clear"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


    <script>
        document.write('<script src=' +
                ('__proto__' in {} ? 'js/vendor/zepto' : 'js/vendor/jquery') +
                '.js><\/script>')
    </script>

    <script src="js/foundation.min.js"></script>

    <!--<script src="js/foundation/foundation.js"></script>

    <script src="js/foundation/foundation.alerts.js"></script>

    <script src="js/foundation/foundation.clearing.js"></script>

    <script src="js/foundation/foundation.cookie.js"></script>

    <script src="js/foundation/foundation.dropdown.js"></script>

    <script src="js/foundation/foundation.forms.js"></script>

    <script src="js/foundation/foundation.joyride.js"></script>

    <script src="js/foundation/foundation.magellan.js"></script>

    <script src="js/foundation/foundation.orbit.js"></script>

    <script src="js/foundation/foundation.reveal.js"></script>

    <script src="js/foundation/foundation.section.js"></script>

    <script src="js/foundation/foundation.tooltips.js"></script>

    <script src="js/foundation/foundation.topbar.js"></script>

    <script src="js/foundation/foundation.interchange.js"></script>

    <script src="js/foundation/foundation.placeholder.js"></script>

    <script src="js/foundation/foundation.abide.js"></script>-->

    <script>
        $(document).foundation();
    </script>
</body>
</html>
