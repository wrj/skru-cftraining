<!------>

<cfquery datasource="cf101mysql" name="result"
    cachedwithin="#CreateTimeSpan(0, 0, 1, 0)#">
    select * from contacts order by contactID asc
</cfquery>

<h3>loop over database query</h3>
<cfdump var="#result#">

<h3>Query of Queries</h3>
<cfquery name="filter" dbtype="query">
    select * from result 
    where email like '%3ddaily%'
</cfquery>
<cfdump var="#filter#">

    
    
    
<cfoutput>
    <ul>
        <cfloop query="result">
                <li><a href="#email#">
                    #firstname# #lastname#
                    </a>
                </li>
        </cfloop>
    </ul>
</cfoutput>

<h3>loop over range</h3>
<cfoutput>
    <cfloop index = "LoopCount" from = "1" to = "5">
        The loop index is #LoopCount#.<br>
    </cfloop>
</cfoutput>

<h3>loop over list</h3>
<cfset Beatle="John,Paul,George,Ringo">
<cfloop index="ListElement" list=#Beatle#> 
    <cfoutput>#ListElement#</cfoutput><br> 
</cfloop>

<h3>loop over array</h3>
<cfset budget = [250,300,350,400]>
<cfloop index="ArrayElement" array=#budget#>
    <cfoutput>#ArrayElement#</cfoutput><br>
</cfloop>





