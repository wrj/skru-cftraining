<cfcomponent>
	<cffunction name="listStudent" returntype="Query" access="public">
		<cfquery name="result" datasource="#application.datasource#">
			select * from student
		</cfquery>
		<cfreturn result>
	</cffunction>

	<cffunction name="getStudent" returntype="Query" access="public">
		<cfargument name="id" type="numeric" required="true">

		<cfquery name="result" datasource="#application.datasource#">
			select * from student where id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.id#">
		</cfquery>
		<cfreturn result>
	</cffunction>

	<cffunction name="updateStudent" returntype="void" access="public">
		<cfargument name="formdata" type="struct" required="true">

		<cfquery name="reslt" datasource="#application.datasource#">
			update student set 
				firstname = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.formdata.firstname#">,
				lastname = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.formdata.lastname#">,
				email = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.formdata.email#">,
				gender = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.formdata.gender#">,
				photo = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.formdata.photo#">
			where
				id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.formdata.id#">
		</cfquery>
	</cffunction>

	<cffunction name="addStudent" returntype="void" access="public">
		<cfargument name="formdata" type="struct" required="true">
		<cfquery name="result" datasource="#application.datasource#">
			insert into student (firstname,lastname,email,gender,photo) values
				(
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.formdata.firstname#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.formdata.lastname#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.formdata.email#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.formdata.gender#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.formdata.photo#">
				)
		</cfquery>
	</cffunction>

	<cffunction name="listCourse" returntype="Query" access="public">
		<cfquery name="result" datasource="#application.datasource#">
			select * from course
		</cfquery>
		<cfreturn result>
	</cffunction>

	<cffunction name="getCourse" returntype="Query" access="public">
		<cfargument name="id" type="numeric" required="true">

		<cfquery name="result" datasource="#application.datasource#">
			select * from course where id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.id#">
		</cfquery>
		<cfreturn result>
	</cffunction>

	<cffunction name="updateCourse" returntype="void" access="public">
		<cfargument name="formdata" type="struct" required="true">

		<cfquery name="reslt" datasource="#application.datasource#">
			update course set 
				name = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.formdata.name#">,
				code = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.formdata.code#">,
				description = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.formdata.description#">
			where
				id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.formdata.id#">
		</cfquery>
	</cffunction>

	<cffunction name="addCourse" returntype="void" access="public">
		<cfargument name="formdata" type="struct" required="true">
		<cfquery name="result" datasource="#application.datasource#">
			insert into course (name, code, description) values
				(
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.formdata.name#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.formdata.code#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.formdata.description#">
				)
		</cfquery>
	</cffunction>

	<cffunction name="getCourseFromStudent" returntype="Query" access="public">
		<cfargument name="id" type="numeric" required="true">
		<cfquery name="result" datasource="#application.datasource#">
			SELECT course.`name`, 
				course.`code`, 
				course.id, 
				course.description, 
				course.`status`,
				coursestudentjoin.student_id
			FROM coursestudentjoin INNER JOIN course ON coursestudentjoin.course_id = course.id
			WHERE coursestudentjoin.student_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.id#">
		</cfquery>
		<cfreturn result>
	</cffunction>

	<cffunction name="getStudentFromCourse" returntype="Query" access="public">
		<cfargument name="id" type="numeric" required="true">
		<cfquery name="result" datasource="#application.datasource#">
			SELECT coursestudentjoin.course_id, 
				student.id, 
				student.firstname, 
				student.lastname, 
				student.email, 
				student.gender, 
				student.photo, 
				student.`status`
			FROM coursestudentjoin INNER JOIN student ON coursestudentjoin.student_id = student.id
			WHERE coursestudentjoin.course_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.id#">
		</cfquery>
		<cfreturn result>
	</cffunction>

	<cffunction name="takeCourse" returntype="void" access="public">
		<cfargument name="student_id" type="numeric" required="true">
		<cfargument name="course_id" type="numeric" required="true">

		<cfquery name="result" datasource="#application.datasource#">
			insert into coursestudentjoin (student_id, course_id) values
				(<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.student_id#">,
				 <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.course_id#">)
		</cfquery>
	</cffunction>

	<cffunction name="deleteCourse" returntype="void" access="public">
		<cfargument name="course_id" type="numeric" required="true">
		
		<cfquery name="result" datasource="#application.datasource#">
			delete from coursestudentjoin
				where course_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.course_id#">
		</cfquery>

		<cfquery name="result" datasource="#application.datasource#">
			delete from course
				where id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.course_id#">
		</cfquery>

	</cffunction>


	<cffunction name="deleteStudent" returntype="void" access="public">
		<cfargument name="student_id" type="numeric" required="true">
		
		<cfquery name="result" datasource="#application.datasource#">
			delete from coursestudentjoin
				where student_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.student_id#">
		</cfquery>

		<cfquery name="result" datasource="#application.datasource#">
			delete from student
				where id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.student_id#">
		</cfquery>

	</cffunction>

	<cffunction name="cancelCourse" returntype="void" access="public">
		<cfargument name="student_id" type="numeric" required="true">
		<cfargument name="course_id" type="numeric" required="true">
		<cfquery name="result" datasource="#application.datasource#">
			delete from coursestudentjoin
				where student_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.student_id#">
				and course_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.course_id#">
		</cfquery>
	</cffunction>
</cfcomponent>