<!DOCTYPE html>
<html>
<head>
	<title>Struct Data type</title>
</head>
<body>
<h1>Struct</h1>
<h3>วิธีการสร้าง Struct</h3>
	<h4>ให้ใช้สัญลักษณ์ {} โดยเป็นการจับคู่ระหว่าง Key และ Value โดยใช้เครื่องหมาย = หรือ : เป็นการกำหนดค่า value ให้แต่ละ Key แล้วคั่นด้วย comma "," </h4>
	<blockquote>
		<pre>
			profile1 = {"firstName" = "Weerasak", "lastName" = "Chongnguluam"};
			profile2 = {"firstName" : "Kanokon", "lastName" : "Chansom"};
		</pre>
	</blockquote>

<h3>วิธีการกำหนดค่า แก้ไขค่า หรือเพิ่มค่า key value ให้กับ struct</h3>
	<h4>ใช้ชื่อตัวแปร struct แล้วตามด้วยสัญลักษณ์ [] โดยข้างในเป็นค่า key แล้วเราสามารถกำหนดค่าอื่นทับลงได้เหมือนตัวแปรอื่นทั่วไป</h4>
	<blockquote>
		<pre>
			profile1["firstname"] = "Paweena";
			profile1["age"] = 30;
			writeOutput(profile1);
		</pre>
	</blockquote>

<h3>การวนลูปไปใน struct สามารถทำได้โดยใช้ tag cfloop หรือถ้าเขียนในแบบ cfscript สามารถใช้ for in loop ช่วยได้</h3>
	<h4>การวนลูปด้วย cfloop</h4>
	<blockquote>
		<pre>		
				&lt;cfloop cllection="#profile1#" item="key"&gt;
					&lt;cfoutput>#profile1[key]#&lt;/cfoutput&gt;
				&lt;/cfloop&gt;
		</pre>
	</blockquote>
	<h4>การวนลูปด้วย for in</h4>
	<blockquote>
		<pre>
			for (key in profile1) {
				writeOutput(profile1[key]);
			}
		</pre>
	</blockquote>

<h3>การเช็คว่าข้อมูลเป็นแบบ Struct หรือไม่</h3>
	<h4>การเช็คว่าข้อมูลเป็นแบบ Struct หรือไม่ใช้ฟังก์ชัน isStruct</h4>
	<blockquote>
		<pre>
			&lt;cfif isStruct(profile1)&gt;
				&lt;cfoutput&gt;It is Struct&lt;/cfoutput&gt;
			&lt;/cfif&gt;
		</pre>
	</blockquote>

<h3>การเช็คว่าตัวแปร struct มี key ที่เราต้องการอยู่หรือไม่</h3>
	<h4>การตรวจสอบว่ามี key อยู่หรือไม่ให้ใช้ฟังก์ชัน structKeyExists</h4>
	<blockquote>
		<pre>
			&lt;cfif structKeyExists(profile1, "firstName")&gt;
				&lt;cfoutput&gt;#profile1['firstName']#&lt;/cfoutput&gt;
			&lt;/cfif&gt;
		</pre>
	</blockquote>
</body>
</html>