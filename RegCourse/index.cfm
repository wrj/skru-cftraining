<!DOCTYPE html>
<!--[if IE 8]> 				 <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">
  <title>Welcome to Register Course System</title>
  <link rel="stylesheet" href="css/foundation.css">
  <link rel="stylesheet" href="responsive-tables.css">
  <script src="js/vendor/custom.modernizr.js"></script>
  <script src="responsive-tables.js"></script>
</head>
<body>
	<div class="row">
		<div class="large-12 columns">
			<h2>Welcome to Register Course System</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi non repellendus suscipit? Aliquam, animi aut consequatur, cum dolorem doloribus excepturi expedita in iusto odit, porro recusandae reprehenderit similique sint ut.</p>
			<hr />

            <a href="add_course.cfm" class="button">New Course</a>
            <a href="add_student.cfm" class="button">New Student</a>
            <a href="take_course.cfm" class="button">Register</a>
            <div class="section-container auto" data-section>
                <section class="active">
                    <p class="title" data-section-title><a href="#panel1">Course</a></p>
                    <div class="content" data-section-content>
                        <table class="responsive">
                            <thead>
                            <tr>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Description</th>
                            </tr>
                            </thead>
                            <tbody>
                            <cfset courses = application.regcourse.listCourse()>
                            <cfoutput query="courses">
							              <tr>
                                <td>#courses.code#</td>
                                <td><a href="course.cfm?id=#courses.id#">#courses.name#</a></td>
                                <td>#courses.description#</td>
                            </tr>
                            </cfoutput>
                            </tbody>
                        </table>
                    </div>
                </section>
                <section>
                    <p class="title" data-section-title><a href="#panel2">Student</a></p>
                    <div class="content" data-section-content>
                        <table class="responsive">
                            <thead>
                            <tr>
                                <th>First name</th>
                                <th>Last name</th>
                            </tr>
                            </thead>
                            <tbody>
                            <cfset students = application.regcourse.listStudent()>
                            <cfoutput query="students">
								              <tr>
	                                <td><a href="student.cfm?id=#students.id#">#students.firstname#</td>
	                                <td>#students.lastname#</td>
	                            </tr>
                            </cfoutput>
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>
	    </div>
	</div>

  <script>
  document.write('<script src=' +
  ('__proto__' in {} ? 'js/vendor/zepto' : 'js/vendor/jquery') +
  '.js><\/script>')
  </script>
  
  <script src="js/foundation.min.js"></script>
  <!--
  
  <script src="js/foundation/foundation.js"></script>
  
  <script src="js/foundation/foundation.alerts.js"></script>
  
  <script src="js/foundation/foundation.clearing.js"></script>
  
  <script src="js/foundation/foundation.cookie.js"></script>
  
  <script src="js/foundation/foundation.dropdown.js"></script>
  
  <script src="js/foundation/foundation.forms.js"></script>
  
  <script src="js/foundation/foundation.joyride.js"></script>
  
  <script src="js/foundation/foundation.magellan.js"></script>
  
  <script src="js/foundation/foundation.orbit.js"></script>
  
  <script src="js/foundation/foundation.reveal.js"></script>
  
  <script src="js/foundation/foundation.section.js"></script>
  
  <script src="js/foundation/foundation.tooltips.js"></script>
  
  <script src="js/foundation/foundation.topbar.js"></script>
  
  <script src="js/foundation/foundation.interchange.js"></script>
  
  <script src="js/foundation/foundation.placeholder.js"></script>
  
  <script src="js/foundation/foundation.abide.js"></script>
  
  -->
  
  <script>
    $(document).foundation();
  </script>
</body>
</html>
