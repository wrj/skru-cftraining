<cfset contact = application.contactApp.getContact(URL.contactID)>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Edit Contact</title>
</head>
<body>
<cfoutput>
    <h1>Edit Contact</h1>
    <form action="do_edit_contact.cfm" method="post">
        <input type="hidden" name="contactID" value="#contact.contactID#">
        <label for="firstName">First Name:</label>
        <input type="text" name="firstName" value="#contact.firstName#"><br>
        <label for="lastName">Last Name:</label>
        <input type="text" name="lastName" value="#contact.lastName#"><br>
        <label for="email">Email:</label>
        <input type="text" name="email" value="#contact.email#"><br>
        <input type="submit" value="Update Contact">
    </form>
</cfoutput>
</body>
</html>