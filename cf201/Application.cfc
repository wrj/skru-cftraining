<cfcomponent>
    <cfset THIS.name = "Contacts" >
    <cfset THIS.datasource = "cf101mysql" >
    <cfset THIS.sessionmanagement = true >
    <cfset THIS.sessiontimeout = createTimeSpan(0, 0, 30, 0) >

    <cffunction name="onRequestStart">
	    <cfargument name="targetPage">
		<cfif not structKeyExists(application, "contactApp") 
			or structKeyExists(url,"reload")>
			<!--- instantiate contactApp instance from Contacts construtor method --->
			<cfset application.contactApp = createObject("Contacts")>
            <!--- <cfset application.contactApp = new Contacts.Contacts()> --->
		</cfif>
		
		<cfif not structKeyExists(application, "datasource")>
			<cfset application.datasource = "cf101mysql">
		</cfif>
	    
	    <cfreturn true>
    </cffunction>

</cfcomponent>