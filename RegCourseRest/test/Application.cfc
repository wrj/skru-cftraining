component {
	this.name = "Register_REST_Test";
	THIS.sessionmanagement = true;
	THIS.sessiontimeout = createTimeSpan(0, 0, 30, 0);
	function onApplicationStart() {
		application.datasource = "regcourse_test";
	}

	function onRequestStart(string targetPage) {
		if (not structKeyExists(application,"datasource")) {
			application.datasource = "regcourse_test";
		}
		return true;
	}
}