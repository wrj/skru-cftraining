component restpath="/course" rest="true" output="false"{
	remote any function listCourse() httpmethod="GET" {

		var result = application.regcourse.listCourse();
		var resultArray = [];
		var course = {};
		for (var index = 1; index <= result.recordCount; index+=1) {
			course = {
				"id" : result.id[index],
				"code" : result.code[index],
				"name" : result.name[index],
				"description" : result.description[index]
			}
			arrayAppend(resultArray,course);
		}
		return resultArray;
	}

	remote any function getCourse(numeric id restargsource="PATH") httpmethod="GET" restpath="{id}" {
		var result = application.regcourse.getCourse(arguments.id);
		if (result.recordCount neq 0) {
			return {
			"id" : result.id,
			"code" : result.code,
			"name" : result.name,
			"description" : result.description
		};
		}
		
		return {};
	}

	remote any function addCourse(
					any code		restargsource="Form",
					any name		restargsource="Form",
					any description restargsource="Form"
				) httpmethod="POST" restpath="add"{
		
		application.regcourse.addCourse({
			'code'	: arguments.code,
			'name'	: arguments.name,
			'description'	: arguments.description
			})
		return true;
	}

	remote any function updateCourse(
					any id 					restargsource="Form",
					any code		restargsource="Form",
					any name		restargsource="Form",
					any description restargsource="Form"
				) httpmethod="POST" restpath="update" {

		application.regcourse.updateCourse({
			'id'		: arguments.id,
			'code'	: arguments.code,
			'name'	: arguments.name,
			'description'	: arguments.description
			})
		return true;
	}

	remote any function deleteCourse(
					any id 					restargsource="PATH"
				) httpmethod="GET" restpath="{id}/delete" {
		application.regcourse.deleteCourse(arguments.id);
		return true;
	}

	remote any function getStudents(
					any id 					restargsource="PATH"
				) httpmethod="GET" restpath="{id}/students" {
		var result = application.regcourse.getStudentFromCourse(arguments.id);
		var resultArray = [];
		var student = {};
		for (var index = 1; index <= result.recordCount; index+=1) {
			student = {
				"id" : result.id[index],
				"firstname" : result.firstname[index],
				"lastname" : result.lastname[index],
				"email" : result.email[index],
				"gender" : result.gender[index],
				"gender" : result.gender[index]
			}
			arrayAppend(resultArray,student);
		}
		return resultArray;
	}
}