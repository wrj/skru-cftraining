<!DOCTYPE html>
<!--[if IE 8]><html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->
<cfset student = application.regcourse.getStudent(URL.id)>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <title>Edit Student</title>
    <link rel="stylesheet" href="css/foundation.css">
    <script src="js/vendor/custom.modernizr.js"></script>
</head>
<body>

<div class="row">
    <div class="large-12 columns">
        <h2>Welcome to Register Course System</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem culpa laudantium nemo numquam perspiciatis qui, sequi ut. Ab aspernatur cupiditate eligendi fugit saepe sunt vitae. Deleniti maiores odio voluptate voluptatum?</p>
        <hr />
        <div class="row">
            <div class="large-8 large-offset-2 columns">
                <h5>Edit Student</h5>
                <div class="form-for-new-student">
                    <form action="do_edit_student.cfm" method="POST" enctype="multipart/form-data">
                        <cfoutput>
                        <label for="firstname">FirstName</label>
                        <input type="hidden" name="id" value="#student.id#">
                        <input type="text" name="firstname" id="firstname" value="#student.firstname#">
                        <label for="lastname">LastName</label>
                        <input type="text" name="lastname" id="lastname" value="#student.lastname#">
                        <label for="email">Email</label>
                        <input type="email" name="email" id="email" value="#student.email#">
                        <label>Gender</label>

                        <cfset maleChecked = "">
                        <cfset femaleChecked = "">
                        <cfif student.gender eq "M">
                            <cfset maleChecked = "checked">
                        <cfelseif student.gender eq "F">
                            <cfset femaleChecked = "checked">
                        </cfif>

                        <input name="gender" type="radio" id="male" value="M" #maleChecked#><span class="custom radio checked" ></span> Male
                        <input name="gender" type="radio" id="female" value="F" #femaleChecked#><span class="custom radio checked"></span> Female
                        <label for="image">Image</label>
                        <input type="hidden" name="photo" value="#student.photo#">
                        <input type="file" name="photoFile" id="image"/>
                        <div style="margin-top: 12px;">
                            <input class="nice radius button" type="submit" name="submitButton" value="Update">
                            <div class="clear"></div>
                        </div>
                        </cfoutput>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

    <script>
        document.write('<script src=' +
                ('__proto__' in {} ? 'js/vendor/zepto' : 'js/vendor/jquery') +
                '.js><\/script>')
    </script>

    <script src="js/foundation.min.js"></script>

    <!--<script src="js/foundation/foundation.js"></script>

    <script src="js/foundation/foundation.alerts.js"></script>

    <script src="js/foundation/foundation.clearing.js"></script>

    <script src="js/foundation/foundation.cookie.js"></script>

    <script src="js/foundation/foundation.dropdown.js"></script>

    <script src="js/foundation/foundation.forms.js"></script>

    <script src="js/foundation/foundation.joyride.js"></script>

    <script src="js/foundation/foundation.magellan.js"></script>

    <script src="js/foundation/foundation.orbit.js"></script>

    <script src="js/foundation/foundation.reveal.js"></script>

    <script src="js/foundation/foundation.section.js"></script>

    <script src="js/foundation/foundation.tooltips.js"></script>

    <script src="js/foundation/foundation.topbar.js"></script>

    <script src="js/foundation/foundation.interchange.js"></script>

    <script src="js/foundation/foundation.placeholder.js"></script>

    <script src="js/foundation/foundation.abide.js"></script>-->


    <script>
        $(document).foundation();
    </script>
</body>
</html>
