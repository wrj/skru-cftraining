<html>
<head>
	<title>Array</title>
</head>
<body>
<h1>Array</h1>
<h3>วิธีสร้าง array</h3>
	<h4>array ว่าง</h4>
		<blockquote>
			<p>&lt;cfset basicarray = [] /&gt;</p>
			<p>หรือ</p>
			<p>&lt;cfset basicarray = ArrayNew() /&gt;</p>
		</blockquote>	
	<h4>array พร้อมค่า value</h4>
		<blockquote>
			<p>&lt;cfset basicarray = ["Warm Sandy Beaches", "Tropical Drinks", 42] /&gt;</p>
		</blockquote>	
<h3>วิธี add value เข้า array</h3>
	<h4>1. กำหนด index ของ array</h4>
		<blockquote>
			<p>&lt;cfset basicarray[1]  = "Warm Sandy Beaches" /&gt;</p>
		</blockquote>	
	<h4>2. ใช้ function ของ coldfusion</h4>
		<h5>1. ArrayAppend</h5>
			<p>เพิ่ม value เข้า array โดยการเพิ่มต่อท้าย array</p>
			<blockquote>
				<p>Function syntax</p>
				<p>ArrayAppend(array, value)</p>
				<p>&lt;cfset ArrayAppend( basicarray,  "Tropical Drinks") /&gt;</p>
			</blockquote>	
		<h5>2. ArrayPrepend</h5>
			<p>เพิ่ม value เข้า array โดยการเพิ่มตัวแรกของ array</p>
			<blockquote>
				<p>Function syntax</p>
				<p>ArrayPrepend(array, value)</p>
				<p>&lt;cfset ArrayPrepend( basicarray,  "Tropical Drinks") /&gt;</p>
			</blockquote>
		<h5>3. ArrayInsertAt</h5>
			<p>เพิ่ม value เข้า array โดยการกำหนด index ของ array</p>
			<blockquote>
				<p>Function syntax</p>
				<p>ArrayInsertAt(array, position, value)</p>
				<p>&lt;cfset ArrayInsertAt( basicarray, 1,  "Tropical Drinks") /&gt;</p>
			</blockquote>	
<h3>วิธี delete value ใน array</h3>
	<h4>ArrayDeleteAt</h4>
		<p>delete value ใน array ตาม index</p>
		<blockquote>
			<p>Function syntax</p>
			<p>ArrayDeleteAt(array, position)</p>
			<p>&lt;cfset ArrayDeleteAt( basicarray,  1) /&gt;</p>
		</blockquote>		
<h3>Function อื่นๆ ที่เกี่ยวกับ array</h3>
	<h4>IsArray</h4>
		<p>เช็คว่าเป็น array หรือไม่</p>
		<blockquote>
			IsArray(array)
			<p>&lt;cfif IsArray( basicarray)&gt;</p>

			<p>&lt;/cfif&gt;</p>
		</blockquote>
	<h4>ArrayIsEmpty</h4>
		<p>เช็ค array ว่าเป็นค่าว่างหรือไม่</p>
		<blockquote>
			ArrayIsEmpty(array)
			<p>&lt;cfif ArrayIsEmpty( basicarray)&gt;</p>

			<p>&lt;/cfif&gt;</p>
		</blockquote>
	<h4>ArrayLen</h4>
		<p>เช็คว่า array มีจำนวนข้อมูลเท่าไร </p>
		<blockquote>
			ArrayLen(array)
			<p>&lt;cfoutput&gt; #ArrayLen( basicarray)#&lt;/cfoutput&gt;</p>
		</blockquote>
	<h4>ArrayMax</h4>
		<p>หาค่า max ใน array โดยข้อมูลต้องเป็นตัวเลขเท่านั้น</p>
		<blockquote>
			ArrayMax(array)
			<p>&lt;cfset basicarray = [1,2,3,4,5] /&gt;</p>
			<p>&lt;cfoutput&gt; #ArrayMax( basicarray)#&lt;/cfoutput&gt;</p>
		</blockquote>
	<h4>ArrayMin</h4>
		<p>หาค่า min ใน array โดยข้อมูลต้องเป็นตัวเลขเท่านั้น</p>
		<blockquote>
			ArrayMin(array)
			<p>&lt;cfset basicarray = [1,2,3,4,5] /&gt;</p>
			<p>&lt;cfoutput&gt; #ArrayMin( basicarray)#&lt;/cfoutput&gt;</p>
		</blockquote>
	<h4>ArrayAvg</h4>
		<p>หาค่าเฉลี่ยใน array โดยข้อมูลต้องเป็นตัวเลขเท่านั้น</p>
		<blockquote>
			ArrayMin(array)
			<p>&lt;cfset basicarray = [1,2,3,4,5] /&gt;</p>
			<p>&lt;cfoutput&gt; #ArrayAvg( basicarray)#&lt;/cfoutput&gt;</p>
		</blockquote>	
	<h4>ArrayToList</h4>
		<p>แปลง array ไปเป็น list</p>
		<blockquote>
			ArrayToList(array [, delimiter ])
			<p>&lt;cfset basiclist = ArrayToList(basicarray,',') /&gt;</p>
		</blockquote>
	<h4>ListToArray</h4>
		<p>แปลง list ไปเป็น array</p>
		<blockquote>
			ListToArray(list [, delimiters, includeEmptyFields])
			<p>&lt;cfset basicarray = ListToArray(basiclist,',',false) /&gt;</p>
		</blockquote>
</body>
</html>