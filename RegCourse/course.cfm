<!DOCTYPE html>
<!--[if IE 8]> 				 <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->

<cfset course = application.regcourse.getCourse(URL.id)>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <title>Foundation 4</title>
    <link rel="stylesheet" href="css/foundation.css">
    <script src="js/vendor/custom.modernizr.js"></script>
</head>
<body>

<div class="row">
    <div class="large-12 columns">
        <h2>Welcome to Register Course System</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid commodi culpa deleniti, deserunt dicta est explicabo fugiat iusto labore molestias nostrum, obcaecati odio omnis pariatur temporibus ullam voluptatibus! Assumenda, esse.</p>
        <hr />
        <a href="add_course.cfm" class="button">New Course</a>
        <a href="add_student.cfm" class="button">New Student</a>
        <a href="take_course.cfm" class="button">Register</a>
        <div class="large-8 large-offset-2 columns">
            <div class="large-3 columns">
                <div class="image" style="height: 100px; width: 100px"></div>
            </div>
            <div class="large-9 columns">
                <cfoutput>
                    <p>#course.code#</p>
                    <p>#course.name#</p>
                    <p>#course.description#</p>
                    <div style="margin-top: 12px;">
                        <a href="edit_course.cfm?id=#course.id#" class="button">Edit</a>
                        <a id="deleteButton" href="##" class="button">Delete</a>
                        <div class="clear"></div>
                    </div>
                </cfoutput>
            </div>
            <table class="large-12">
                <thead>
                <tr>
                    <th>First name</th>
                    <th>Last name</th>
                </tr>
                </thead>
                <tbody>
                <cfset students = application.regcourse.getStudentFromCourse(course.id)>
                <cfoutput query="students">
                    <tr>
                        <td><a href="student.cfm?id=#students.id#">#students.firstname#</td>
                        <td>#students.lastname#</td>
                    </tr>
                </cfoutput>
                </tbody>                
            </table>
        </div>
    </div>
</div>

<script>
    document.write('<script src=' +
            ('__proto__' in {} ? 'js/vendor/zepto' : 'js/vendor/jquery') +
            '.js><\/script>')
</script>

<script src="js/foundation.min.js"></script>
<!--

<script src="js/foundation/foundation.js"></script>

<script src="js/foundation/foundation.alerts.js"></script>

<script src="js/foundation/foundation.clearing.js"></script>

<script src="js/foundation/foundation.cookie.js"></script>

<script src="js/foundation/foundation.dropdown.js"></script>

<script src="js/foundation/foundation.forms.js"></script>

<script src="js/foundation/foundation.joyride.js"></script>

<script src="js/foundation/foundation.magellan.js"></script>

<script src="js/foundation/foundation.orbit.js"></script>

<script src="js/foundation/foundation.reveal.js"></script>

<script src="js/foundation/foundation.section.js"></script>

<script src="js/foundation/foundation.tooltips.js"></script>

<script src="js/foundation/foundation.topbar.js"></script>

<script src="js/foundation/foundation.interchange.js"></script>

<script src="js/foundation/foundation.placeholder.js"></script>

<script src="js/foundation/foundation.abide.js"></script>

-->

<script>
    $(document).foundation();
    $("#deleteButton").on("click", function() {
        if (confirm("ต้องการลบ course ใช่หรือไม่?")) {
            <cfoutput>
                location.href="delete_course.cfm?id=#course.id#"
            </cfoutput>
        }
    })
</script>
</body>
</html>
