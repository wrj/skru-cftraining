<cfcomponent>
    <cffunction name="listContacts" returntype="Query" access="public">
		<cfquery name="result" datasource="#application.datasource#">
			select * from contacts order by contactID asc
		</cfquery>
		<cfreturn result>
	</cffunction>
    
	<cffunction name="getContact" returntype="Query" access="public">
		<cfargument name="contactID" type="numeric" required="true">

		<cfquery name="result" datasource="#application.datasource#">
			select * from contacts where contactID = 
			<cfqueryparam cfsqltype="cf_sql_integer" 
			    value="#arguments.contactID#">
		</cfquery>
		<cfreturn result>
	</cffunction>
        
    <cffunction name="addContact" returntype="void" access="public">
        <cfargument name="formdata" type="struct" required="true" >
        <cfquery name="result" datasource="#application.datasource#">
            insert into contacts (firstName, lastName, email) values
            (
                <cfqueryparam cfsqltype="cf_sql_varchar"
                 value="#arguments.formdata.firstName#">,
                <cfqueryparam cfsqltype="cf_sql_varchar"
                 value="#arguments.formdata.lastName#">,
                <cfqueryparam cfsqltype="cf_sql_varchar"
                 value="#arguments.formdata.email#">
            )
        </cfquery>
    </cffunction>
    
	<cffunction name="updateContact" returntype="void" access="public">
		<cfargument name="formdata" type="struct" required="true">

		<cfquery name="result" datasource="#application.datasource#">
			update contacts set 
				firstName = <cfqueryparam cfsqltype="cf_sql_varchar"
				 value="#arguments.formdata.firstName#">,
				lastName = <cfqueryparam cfsqltype="cf_sql_varchar"
				 value="#arguments.formdata.lastName#">,
				email = <cfqueryparam cfsqltype="cf_sql_varchar"
				 value="#arguments.formdata.email#">
			where
				contactID = <cfqueryparam cfsqltype="cf_sql_integer"
				 value="#arguments.formdata.contactID#">
		</cfquery>
	</cffunction>
    
    <cffunction name="deleteContact" returntype="void" access="public">
		<cfargument name="contactID" type="numeric" required="true">
		<cfquery name="result" datasource="#application.datasource#">
			delete from contacts
				where contactID = <cfqueryparam cfsqltype="cf_sql_integer"
				 value="#arguments.contactID#">
		</cfquery>
	</cffunction>
  

    <cffunction name="listContacts" access="remote" returntype="string" output="no">        
        <cfset Var ContactList = "">
        <cfset Var getContacts = "">
        <cfquery name="getContacts" datasource="#application.datasource#">
            SELECT *
            FROM contacts
            ORDER BY contactID asc
        </cfquery>
        
        <cfsavecontent variable="ContactList">
          <contacts>
            <cfoutput query="getContacts">
              <contact id="#contactID#">
                <firstname>#XMLFormat(FirstName)#</firstname>
                <lastname>#XMLFormat(LastName)#</lastname>
                <email>#XMLFormat(Email)#</email>
              </contact>
            </cfoutput>
          </contacts>
        </cfsavecontent>
        
        <cfreturn ContactList>
    </cffunction>
        
</cfcomponent>