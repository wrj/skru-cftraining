<cfquery datasource="cf101mysql" name="result"
    cachedwithin="#CreateTimeSpan(0, 0, 1, 0)#">
    select * from contacts order by contactID asc
</cfquery>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Query No Cache</title>
</head>
<body>
    <cfoutput>
        <ul>
            <cfloop query="result">
                    <li><a href="#email#">
                        #firstname# #lastname#
                        </a>
                    </li>
            </cfloop>
        </ul>
    </cfoutput>
    
</body>
</html>