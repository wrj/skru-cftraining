<cfif form.photoFile neq "">
	<cffile 
	action="upload" 
	destination="#expandPath('.')#/img/"
	filefield="photoFile"
	result="uploadResult"
	nameconflict="makeunique">	
	<cfset form['photo'] = uploadResult.serverFile />
</cfif>

<cfset application.regcourse.updateStudent(form) />
<cflocation url="index.cfm" addtoken="false">