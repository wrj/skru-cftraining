<cfcomponent>
	<cfset THIS.name = "Register"/>
	<cfset THIS.datasource = "regcourse"/>
	<cfset THIS.sessionmanagement = true/>
	<cfset THIS.sessiontimeout = createTimeSpan(0, 0, 30, 0)/>

	<cffunction name="onRequestStart">
		<cfargument name="targetPage">
			<cfif not structKeyExists(application, "regcourse") or structKeyExists(url,"reload")>
				<!--- instantiate regcourse instant from RegCourse construtor method --->
				<cfset application.regcourse = new RegCourse.RegCourse()>

			</cfif>
			<cfif not structKeyExists(application, "datasource")>
				<cfset application.datasource = "regcourse">
			</cfif>
		<cfreturn true>
	</cffunction>
</cfcomponent>