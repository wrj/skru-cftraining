<!DOCTYPE html>
<!--[if IE 8]> 				 <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->

<cfset student = application.regcourse.getStudent(URL.id)>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <title>Foundation 4</title>
    <link rel="stylesheet" href="css/foundation.css">
    <script src="js/vendor/custom.modernizr.js"></script>
</head>
<body>

<div class="row">
    <div class="large-12 columns">
        <h2>Welcome to Register Course System</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid commodi culpa deleniti, deserunt dicta est explicabo fugiat iusto labore molestias nostrum, obcaecati odio omnis pariatur temporibus ullam voluptatibus! Assumenda, esse.</p>
        <hr />
        <a href="add_course.cfm" class="button">New Course</a>
        <a href="add_student.cfm" class="button">New Student</a>
        <a href="take_course.cfm" class="button">Register</a>

        <div class="large-8 large-offset-2 columns">
            <div class="large-3 columns">
                <div class="image" style="height: 100px; width: 100px">
                    <cfif student.photo neq ''>
                        <cfoutput><img src="img/#student.photo#" style="width:100px;height:100px"></cfoutput>    
                    </cfif>
                    
                </div>
            </div>
            <div class="large-9 columns">
                <cfoutput>
                    <p>#student.firstname# #student.lastname#</p>
                    <p>#student.email#</p>
                    <p>Gender: #student.gender#</p>
                
                    <div style="margin-top: 12px;">
                        <a href="edit_student.cfm?id=#student.id#" class="button">Edit</a>
                        <a id="deleteButton" href="##" class="button">Delete</a>
                        <div class="clear"></div>
                    </div>
                </cfoutput>
            </div>
            <table class="large-12">
                <thead>
                <tr>
                    <th>Code</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <cfset courses = application.regcourse.getCourseFromStudent(student.id)>
                <cfoutput query="courses">
                <tr>
                    <td>#courses.code#</td>
                    <td><a href="course.cfm?id=#courses.id#">#courses.name#</a></td>
                    <td>#courses.description#</td>
                    <td><a href="cancel_course.cfm?student_id=#student.id#&course_id=#courses.id#">ถอน</a></td>
                </tr>
                </cfoutput>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    document.write('<script src=' +
            ('__proto__' in {} ? 'js/vendor/zepto' : 'js/vendor/jquery') +
            '.js><\/script>')
</script>

<script src="js/foundation.min.js"></script>
<!--

<script src="js/foundation/foundation.js"></script>

<script src="js/foundation/foundation.alerts.js"></script>

<script src="js/foundation/foundation.clearing.js"></script>

<script src="js/foundation/foundation.cookie.js"></script>

<script src="js/foundation/foundation.dropdown.js"></script>

<script src="js/foundation/foundation.forms.js"></script>

<script src="js/foundation/foundation.joyride.js"></script>

<script src="js/foundation/foundation.magellan.js"></script>

<script src="js/foundation/foundation.orbit.js"></script>

<script src="js/foundation/foundation.reveal.js"></script>

<script src="js/foundation/foundation.section.js"></script>

<script src="js/foundation/foundation.tooltips.js"></script>

<script src="js/foundation/foundation.topbar.js"></script>

<script src="js/foundation/foundation.interchange.js"></script>

<script src="js/foundation/foundation.placeholder.js"></script>

<script src="js/foundation/foundation.abide.js"></script>

-->

<script>
    $(document).foundation();
    $("#deleteButton").on("click", function() {
        if (confirm("ต้องการลบ student ใช่หรือไม่?")) {
            <cfoutput>
                location.href="delete_student.cfm?id=#student.id#"
            </cfoutput>
        }
    })
</script>
</body>
</html>
