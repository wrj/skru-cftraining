<html>
<head>
	<title>Query</title>
</head>
<body>
<h1>Query</h1>
	<blockquote>
		sql query
		<p>&lt;cfquery name="result" datasource="datasource"&gt;</p>
			<p>SELECT * FROM books</p>
		<p>&lt;/cfquery&gt;</p><br>
		<p>แสดงผลทุก record</p>
		<p>&lt;cfloop query="result"&gt;</p>
			<p>No : #currentRow#</p>
			<p>Title : #title#</p>
			<p>Author : #currentRow#</p>
		<p>&lt;/cfloop&gt;</p><br>
		<p>หรือ เริ่ม record ที่ 20 จำนวน 10 record</p>
		<p>&lt;cfloop query="result" startrow="20" maxrows="10"&gt;</p>
			<p>No : #currentRow#</p>
			<p>Title : #title#</p>
			<p>Author : #currentRow#</p>
		<p>&lt;/cfloop&gt;</p>
	</blockquote>
	

	<h3>currentRow</h3>
		<blockquote>
			<p>บอกตำแหน่ง index</p>
			<p>&lt;cfloop query="result"&gt;</p>
				<p>&lt;cfoutput&gt;</p>
					<p>#currentRow#</p>
				<p>&lt;/cfoutput&gt;</p>
			<p>&lt;/cfloop&gt;</p>
		</blockquote>
	<h3>columnList</h3>
		<blockquote>
			<p>แสดง fields ของ query</p>
			<p>&lt;cfset databasefield = result.columnList&gt;</p>
		</blockquote>
	<h3>RecordCount</h3>
		<blockquote>
			<p>จำนวนของ query</p>
			<p>&lt;cfoutput&gt;#result.RecordCount#&lt;/cfoutput&gt;</p>
		</blockquote>
</body>
</html>