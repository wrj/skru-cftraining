<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add Contact</title>
</head>
<body>
    <h1>Add Contact</h1>
    <form action="do_add_contact.cfm" method="post">
        <label for="firstName">First Name:</label>
        <input type="text" name="firstName"><br>
        <label for="lastName">Last Name:</label>
        <input type="text" name="lastName"><br>
        <label for="email">Email:</label>
        <input type="text" name="email"><br>
        <input type="submit" value="Add Contact">
    </form>
</body>
</html>